(ns quipper.core
  (require [clojure.tools.cli :as cli])
  (:gen-class))

(def cli-options [["-h" "--help" "Print this help info"
                   :default false :flag true]
                  ["-f" "--file FILE" "Specify an alternate quip file."
                   :default (str
                             (System/getProperty "user.home")
                             "/.quips")]])

(def delimiter "######")

(defn get-quips [filename]
  (let 
    [file-contents  (slurp filename)]
    (if
      (= 0 (count (clojure.string/trim file-contents)))
      []
      (clojure.string/split file-contents (re-pattern delimiter)))
  )
)

(defn get-quip [filename]
  (let 
    [quips  (get-quips filename)]
    (if
      (= 0 (count quips))
      ""
      (rand-nth quips))
  )
)

(defn add-quip [filename quips]
    (let
      [quips    (clojure.string/join delimiter quips)] 
      (spit filename (str quips delimiter) :append true))
)

(defn get-count [filename]
  (let 
    [quips  (get-quips filename)]
    (count quips)
  )
)

(defn drop-quips [filename]
  (spit filename "")
)

(defn -main
  "Deal with quips."
  [& args]
  (let [{:keys [options arguments summary errors]} (cli/parse-opts args cli-options)
        help-output (str "This is the 'quipper' program.\n" summary)]
        (if 
          (options :help) 
          (println help-output) 
          (cond
            (or (= 0 (count arguments)) (= "get" (first arguments)))
              (println (get-quip (options :file)))
            (= "add" (first arguments))
              (if
                (> (count arguments) 1) 
                (add-quip (options :file) (rest arguments)) 
                (println help-output))
            (= "drop" (first arguments))
              (drop-quips (options :file))
            (= "count" (first arguments))
              (println (get-count (options :file)))
          )))) 
